/*eslint-disable */

const TestVerwaltungStore = {
     state: {
    TestVerwaltung_Bilder:[
             {id:1,name:"Apfel",url:"https://upload.wikimedia.org/wikipedia/commons/1/11/Sterappel_dwarsdrsn.jpg"},
             {id:2,name:"Orange",url:"https://upload.wikimedia.org/wikipedia/commons/b/b0/OrangeBloss_wb.jpg"}
         ]
 },
 mutations: {
    LOAD_PICTURES(state){
        
    }
 },
 actions: {
    loadPictures({commit}){     
      commit('LOAD_PICTURES')
    }
 },
 getters:{     
     pictures : state => {return state.TestVerwaltung_Bilder}
 }
}
export default TestVerwaltungStore