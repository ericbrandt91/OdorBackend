/*eslint-disable */

const AuthStore = {
state: {
    authenticated:false,
    lock: new Auth0Lock('rY7Var7VQokTLn5mTbh8Ty5bs7ykGoB6', 'twin322.auth0.com',{
         auth:{
           responseType:'token',  
           redirect:true,  
           params:{
             scope:'openid email'
           } 
         },
      
         allowSignUp: true,
         language:"de",
         title:"Eric",
         languageDictionary:{
            title:"Odor Login"
         },
         theme:{
                primaryColor:"green",
             logo:"http://www.logopaedie-schule-kreischa.de/assets/images/uniklinik.gif"      
             
         }
    })
 },
 mutations: {
    LOGIN(state){
        state.lock.show();
    },
    INIT(state,router){
        localStorage.removeItem('id_token');
        localStorage.removeItem('profile');        
        state.authenticated = false;
      state.lock.on('authenticated', (authResult) => {
      console.log('authenticated');
      localStorage.setItem('id_token', authResult.idToken);
      state.lock.getProfile(authResult.idToken, (error, profile) => {
        if (error) {
          console.log("Error")
          return;
        }
        // Set the token and user profile in local storage
        localStorage.setItem('profile', JSON.stringify(profile));
        
        state.authenticated = true;
        console.log("auth: ")
           router.push('/home')
        
      });
    });
    state.lock.on('authorization_error', (error) => {
      // handle error when authorizaton fails
    });
    },
    LOGOUT(state){
        localStorage.removeItem('id_token');
        localStorage.removeItem('profile'); 
       // state.lock.logout({ returnTo: 'www.google.de' });       
        state.authenticated = false;        
      
    }
 },
 actions: {
    login({commit}){
     
      commit('LOGIN')

    },
    init({commit},router){
        commit('INIT',router)
    },
    logout({commit},router){
        router.push('/')
        commit('LOGOUT')       
    }

 },
 getters:{
     isAuthenticated: state => {return state.authenticated},
     lock : state => {return state.lock}
 }
}
export default AuthStore