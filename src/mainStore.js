/*eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import TestVerwaltung from './TestVerwaltungStore'
import Authentifizierung from './AuthStore'
Vue.use(Vuex)

export default new Vuex.Store({
    	
        modules:{
            a:TestVerwaltung,
            b:Authentifizierung
        }
})